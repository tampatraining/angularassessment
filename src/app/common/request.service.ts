import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Starship } from '../starship';
import { Person } from '../person';
import { Species } from '../species';
import { Vehicle } from '../vehicle';
import { Planet } from '../planet';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private httpClient:HttpClient) { }

  getStarship(selectionType: string, selectionId: number): Observable<Starship>{
      console.log("Calling:" + 'https://swapi.co/api/' + selectionType + '/' + selectionId);
      return this.httpClient.get<Starship>('https://swapi.co/api/' + selectionType + '/' + selectionId);
  }
  getPerson(selectionType: string, selectionId: number): Observable<Person>{
    console.log("Calling:" + 'https://swapi.co/api/' + selectionType + '/' + selectionId);
    return this.httpClient.get<Person>('https://swapi.co/api/' + selectionType + '/' + selectionId);
  }
  getSpecies(selectionType: string, selectionId: number): Observable<Species>{
    console.log("Calling:" + 'https://swapi.co/api/' + selectionType + '/' + selectionId);
    return this.httpClient.get<Species>('https://swapi.co/api/' + selectionType + '/' + selectionId);
  }
  getVehicle(selectionType: string, selectionId: number): Observable<Vehicle>{
    console.log("Calling:" + 'https://swapi.co/api/' + selectionType + '/' + selectionId);
    return this.httpClient.get<Vehicle>('https://swapi.co/api/' + selectionType + '/' + selectionId);
  }
  getPlanet(selectionType: string, selectionId: number): Observable<Planet>{
    console.log("Calling:" + 'https://swapi.co/api/' + selectionType + '/' + selectionId);
    return this.httpClient.get<Planet>('https://swapi.co/api/' + selectionType + '/' + selectionId);
  }
}


