import { Component, OnInit } from '@angular/core';
import { RequestService } from '../common/request.service';
import { Person } from '../person';
import { Starship } from '../starship';
import { Vehicle } from '../vehicle';
import { Species } from '../species';
import { Planet } from '../planet';
import { Result } from '../result';

@Component({
  selector: 'app-selection-bar',
  templateUrl: './selection-bar.component.html',
  styleUrls: ['./selection-bar.component.css']
})
export class SelectionBarComponent implements OnInit {

  categoryList = ["people","planets","vehicles","species","starships"];

  idList = [1,2,3,4,5];

  selectedCategory: string;
  selectedId:number;

  submitted = false;

  tempResponse:string;
  constructor(private requestService:RequestService) { }

  person:Person;
  starship:Starship;
  vehicle:Vehicle;
  species:Species;
  planet:Planet;
  result:Result;
  that = this;

  receivedValue:string;

  ngOnInit() {
  }

  onSubmit(){

    switch(this.selectedCategory) { 
      case "people": { 
        // this.requestService.getPerson(this.selectedCategory,this.selectedId).subscribe(response =>
        // {this.person = response});
        // console.log(this.person);    //this.person is not set yet here because observable hasn't returned
        // this.receivedValue = this.person.name;
        this.requestService.getPerson(this.selectedCategory,this.selectedId).subscribe(response =>
          { this.person = response
            console.log(this.person);
            this.receivedValue = this.person.name;
          });
         break; 
      } 
      case "vehicles": { 
        this.requestService.getVehicle(this.selectedCategory,this.selectedId).subscribe(response =>
          {this.vehicle = response});
        console.log(this.vehicle);
        this.receivedValue = this.vehicle.name;
         break; 
      } 
      case "planets": { 
        this.requestService.getPlanet(this.selectedCategory,this.selectedId).subscribe(response =>
          {this.planet = response});
          console.log(this.planet);
          this.receivedValue = this.planet.name;
         break; 
      } 
      case "starships": { 
        this.requestService.getStarship(this.selectedCategory,this.selectedId).subscribe(response =>
          {this.starship = response});
          console.log(this.starship);
          this.receivedValue = this.starship.name;
         break; 
      } 
      case "species": { 
        this.requestService.getSpecies(this.selectedCategory,this.selectedId).subscribe(response =>
          {this.species = response});
          console.log(this.species);
          this.receivedValue = this.species.name;
         break; 
      } 
   } 
    this.submitted = true;
    console.log("TEST");
    console.log(this.tempResponse);
    console.log(this.selectedCategory);
    console.log(this.selectedId);
  
  }

}
